<?php
declare(strict_types=1);

namespace App\Database\Entities\MailChimp;

use Doctrine\Common\Collections\ArrayCollection;
use Illuminate\Support\Str;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class MailChimpMember extends MailChimpEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     *
     * @var string
     */
    private $memberId;

    /**
     * @ORM\Column(name="email_address", type="string")
     *
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(name="status", type="string")
     *
     * @var string
     */
    private $status;

    /**
     * @ORM\Column(name="mail_chimp_response_id", type="string", nullable=true)
     *
     * @var string
     */
    private $mailChimpResponseId;

    /**
     * @ORM\ManyToMany(targetEntity="MailChimpList", mappedBy="members", cascade={"remove"})
     * @ORM\JoinTable(
     *     name="member_list",
     *     joinColumns={
     *          @ORM\JoinColumn(name="member_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(name="list_id", referencedColumnName="id")
     *     }
     * )
     *
     * @var ArrayCollection|MailChimpList[]
     */
    private $lists;

    /**
     * MailChimpMember constructor.
     * @param array|null $data
     */
    public function __construct(?array $data = null)
    {
        parent::__construct($data);

        $this->lists = new ArrayCollection();
    }

    /**
     * Get validation rules for mailchimp entity.
     *
     * @return array
     */
    public function getValidationRules(): array
    {
        return [
            'email_address' => 'required|string',
            'status' => 'required|string',
        ];
    }

    /**
     * Get array representation of entity.
     *
     * @return array
     */
    public function toArray(): array
    {
        $out = [];

        foreach (get_object_vars($this) as $property => $value) {
            $out[Str::snake($property)] = $value;
        }

        return $out;
    }

    /**
     * @return string
     */
    public function getMemberId(): string
    {
        return $this->memberId;
    }

    /**
     * @param string $memberId
     * @return MailChimpMember
     */
    public function setMemberId(string $memberId): MailChimpMember
    {
        $this->memberId = $memberId;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return MailChimpMember
     */
    public function setEmail(string $email): MailChimpMember
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return MailChimpMember
     */
    public function setStatus(string $status): MailChimpMember
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getMailChimpResponseId(): string
    {
        return $this->mailChimpResponseId;
    }

    /**
     * @param string $mailChimpResponseId
     * @return MailChimpMember
     */
    public function setMailChimpResponseId(string $mailChimpResponseId): MailChimpMember
    {
        $this->mailChimpResponseId = $mailChimpResponseId;
        return $this;
    }

    /**
     * Get all lists that a member is assigned
     *
     * @return MailChimpList[]|ArrayCollection
     */
    public function getLists()
    {
        return $this->lists;
    }

    /**
     * Adds a member to a list
     *
     * @param MailChimpList $mailChimpList
     * @return $this
     */
    public function addToList(MailChimpList $mailChimpList)
    {
        if (! $this->lists->contains($mailChimpList)) {
            $this->lists[] = $mailChimpList;
            $mailChimpList->addMember($this);
        }

        return $this;
    }

    /**
     * Remove a member from a list
     *
     * @param MailChimpList $mailChimpList
     * @return $this
     */
    public function removeFromList(MailChimpList $mailChimpList)
    {
        if ($this->lists->contains($mailChimpList)) {
            $this->lists->removeElement($mailChimpList);
            $mailChimpList->removeMember($this);
        }

        return $this;
    }
}