<?php
namespace App\Http\Controllers\MailChimp;

use App\Database\Entities\MailChimp\MailChimpList;
use App\Database\Entities\MailChimp\MailChimpMember;
use App\Http\Controllers\Controller;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Mailchimp\Mailchimp;
use Exception;

class MembersController extends Controller
{
    /**
     * @var \Mailchimp\Mailchimp
     */
    private $mailChimp;
    
    /**
     * ListsController constructor.
     *
     * @param \Doctrine\ORM\EntityManagerInterface $entityManager
     * @param \Mailchimp\Mailchimp $mailchimp
     */
    public function __construct(EntityManagerInterface $entityManager, Mailchimp $mailchimp)
    {
        parent::__construct($entityManager);
        
        $this->mailChimp = $mailchimp;
    }
    
    /**
     * Create a member, add the member to a list
     *
     * @param Request $request
     * @param string $listId
     * @return JsonResponse
     */
    public function create(Request $request, string $listId): JsonResponse
    {
        /** @var \App\Database\Entities\MailChimp\MailChimpList $list */
        $list = $this->entityManager->getRepository(MailChimpList::class)->find($listId);
        
        // Check whether a list exists with the Id
        if (is_null($list)) {
            return $this->errorResponse([
                'message' => sprintf('MailChimp List[%s] not found', $listId)
            ], 404);
        }
        
        // Add members to lists
        $member = new MailChimpMember($request->all());
        // MailChimp Response Id of a list
        $mailChimpListId = $list->getMailChimpId();
        // Validate entity
        $validator = $this->getValidationFactory()->make($member->toMailChimpArray(), $member->getValidationRules());
        
        // Return error response if validation fails
        if ($validator->fails()) {
            return $this->errorResponse([
                'message' => 'Invalid data given',
                'errors' => $validator->errors()->toArray()
            ]);
        }
        
        try {
            // Save a member into DB
            $list->addMember($member);
            
            // Send a request to add the member to a list
            $response = $this->mailChimp->post(
                sprintf('lists/%s/members', $mailChimpListId),
                $member->toMailChimpArray()
            );
            
            // Set MailChimp Id on the members and save member into db
            $this->saveEntity($member->setMailChimpResponseId($response->get('id')));
        } catch (Exception $exception) {
            // Return error response if something goes wrong
            return $this->errorResponse(['message' => $exception->getMessage()]);
        }
        
        return $this->successfulResponse($member->toArray());
    }
    
    /**
     * Show a single resource of MailChimp Member of a List
     *
     * @param string $memberId
     * @return JsonResponse
     */
    public function show(string $memberId): JsonResponse
    {
        /** @var \App\Database\Entities\MailChimp\MailChimpMember $member */
        $member = $this->entityManager->getRepository(MailChimpMember::class)->find($memberId);
        
        if (is_null($member)) {
            return $this->errorResponse([
                'message' => sprintf('MailChimp Member[%s] not found', $memberId)
            ], 404);
        }
        
        return $this->successfulResponse($member->toArray());
    }
    
    /**
     * Update an existing resource of member of a list
     *
     * @param Request $request
     * @param string $listId
     * @param string $memberId
     * @return JsonResponse
     */
    public function update(Request $request, string $listId, string $memberId): JsonResponse
    {
        /** @var \App\Database\Entities\MailChimp\MailChimpList $list */
        $list = $this->entityManager->getRepository(MailChimpList::class)->find($listId);
    
        // Check whether a list exists with the Id
        if (is_null($list)) {
            return $this->errorResponse([
                'message' => sprintf('MailChimp List[%s] not found', $listId)
            ], 404);
        }
    
        /** @var \App\Database\Entities\MailChimp\MailChimpMember $member */
        $member = $this->entityManager->getRepository(MailChimpMember::class)->find($memberId);
    
        if (is_null($member)) {
            return $this->errorResponse([
                'message' => sprintf('MailChimp Member[%s] not found', $memberId)
            ], 404);
        }
        
        // Update member properties
        $member->fill($request->all());
    
        // Validate entity
        $validator = $this->getValidationFactory()->make($member->toMailChimpArray(), $member->getValidationRules());
    
        // Return error response if validation fails
        if ($validator->fails()) {
            return $this->errorResponse([
                'message' => 'Invalid data given',
                'errors' => $validator->errors()->toArray()
            ]);
        }
    
        // MailChimp Response Id of a list
        $mailChimpListId = $list->getMailChimpId();
        // MailChimp Response Id of a member
        $mailChimpMemberId = $member->getMailChimpResponseId();
        
        try {
            // Update the existing member in the database
            $this->saveEntity($member);
            // Send a UPDATE request to MailChimp API
            $this->mailChimp->patch(
                sprintf('lists/%s/members/%s', $mailChimpListId, $mailChimpMemberId),
                $member->toMailChimpArray()
            );
        } catch (Exception $exception) {
            return $this->errorResponse([
                'message' => $exception->getMessage()
            ]);
        }
        
        return $this->successfulResponse($member->toArray());
    }
    
    public function remove(string $listId, string $memberId): JsonResponse
    {
        /** @var \App\Database\Entities\MailChimp\MailChimpList $list */
        $list = $this->entityManager->getRepository(MailChimpList::class)->find($listId);
    
        // Check whether a list exists with the Id
        if (is_null($list)) {
            return $this->errorResponse([
                'message' => sprintf('MailChimp List[%s] not found', $listId)
            ], 404);
        }
    
        /** @var \App\Database\Entities\MailChimp\MailChimpMember $member */
        $member = $this->entityManager->getRepository(MailChimpMember::class)->find($memberId);
    
        if (is_null($member)) {
            return $this->errorResponse([
                'message' => sprintf('MailChimp Member[%s] not found', $memberId)
            ], 404);
        }
    
        // MailChimp Response Id of a list
        $mailChimpListId = $list->getMailChimpId();
        // MailChimp Response Id of a member
        $mailChimpMemberId = $member->getMailChimpResponseId();
        
        try {
            // Remove from the member from the db
            $this->removeEntity($member);
            
            // Remove the record from the pivot/associated table
            $list->removeMember($member);
            
            // Send a delete request to MailChimp API to delete a member from a list
            $this->mailChimp->delete(
                sprintf('lists/%s/members/%s', $mailChimpListId, $mailChimpMemberId)
            );
        } catch (Exception $exception) {
            return $this->errorResponse([
                'message' => $exception->getMessage()
            ]);
        }
    }
}